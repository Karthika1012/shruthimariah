export APP_PORT=9046
export APP_IMAGE=springbootjspmvccrud
export APP_CONTAINER=springbootjspmvccrud
export JAR_FILENAME=springbootjspmvccrud-0.0.1-SNAPSHOT.jar

export DOCKER_REGISTRY_URL=dockerreg.training.local:5000