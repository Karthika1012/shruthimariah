<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
table, td, th {
	border: 1px solid black;
}
</style>
<title>Add Shipper Detail</title>
</head>
<body style="background-color:aqua">
<form action="/add" method="post">
<table>
<tr>
	<th colspan="2">New Shipper</th>
</tr>
<tr>
	<td>Name: </td><td><input name="shipper" id="shipper"/></td>
</tr>
<tr>
	<td>Phone: </td><td><input name="phone" id="phone" /></td>
</tr>
<tr>
	<td></td><td><input type="submit" value="Save" /></td>
</tr>
</table>
</form>
</body>
</html>