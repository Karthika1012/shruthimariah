<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style>
table, td, th {
	border: 1px solid black;
}
</style>
<title>Northwind Shippers</title>
</head>
<body style="background-color:aqua">
<h2>Northwind Shippers</h2>
<p><a href="/">Home Page</a></p>
<table>
<tr>
	<th>Company</th><th>Phone</th><th>Edit</th><th>Delete</th>
</tr>
<c:forEach var="item" items="${shippers}">
	<tr>
		<td>${item.name}</td>
		<td>${item.phone}</td>
		<td><a href="/edit?id=${item.id}">Edit</a></td>
		<td><a href="/delete?id=${item.id }" 
		onclick="return confirm('Are you sure you want to delete this shipper?')">Delete</a></td>
	</tr>
</c:forEach>
</table>
</body>
</html>