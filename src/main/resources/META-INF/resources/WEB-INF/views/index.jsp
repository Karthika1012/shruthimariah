<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<html>
<head>
	<title>CRUD Spring MVC Demo</title>
</head>
<body style="background-color:aqua">
	<h1>Home Page</h1>
	<a href="/shippers">Shipper List</a><br>
	<a href="/new">Add Shipper</a>
	<p>Message: <b>${msg}</b></p>
	<hr>
	<p>Page generated at <%= new java.util.Date() %></p>
</body>
</html>