package com.example;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.business.*;

@Controller
public class MyController {

	@Autowired
	ShipperRepository repo;
	@Autowired
	clientrepo crepo;
	
    @RequestMapping("/")
    public String handler(Model model) {
        model.addAttribute("msg", "Spring Boot web app, JAR packaging, JSP views");
        return "index";
    }
    
    @RequestMapping("/shippers")
    public String shandler(Model model) {
    	List<Shipper> shippers = repo.allShippers();
        model.addAttribute("shippers", shippers);
        return "shippers";
    }
    
    @RequestMapping("/bookdeal")
    public String bookhandler(HttpServletRequest request,Model model, HttpSession session) {       	
        model.addAttribute("pair",crepo.gettradeprices(request.getParameter("CurrencyPairs")));
        session.setAttribute("currencypair", request.getParameter("CurrencyPairs"));
        model.addAttribute("cpair",request.getParameter("CurrencyPairs"));
        return "bookdeal1";
    }
    
    @RequestMapping("/getrate")
    public String printcurrency(HttpServletRequest request,Model model,HttpSession session) {    
    	model.addAttribute("finalrate",crepo.getfinalrate((String)session.getAttribute("currencypair"),request.getParameter("buysell"),
    			request.getParameter("currency"),Double.parseDouble(request.getParameter("amount"))));
    	session.setAttribute("dbuysell", request.getParameter("buysell"));
    	session.setAttribute("dcurrency", request.getParameter("currency"));
    	session.setAttribute("damount", Double.parseDouble(request.getParameter("amount")));
  
    	session.setAttribute("dclientid", request.getParameter("ClientID"));
    	
    	return "getrate1";
    }
    
    @RequestMapping("/add1")
    public String addclientdata(HttpServletRequest request,Model model,HttpSession session) {  
    	//session.setAttribute("dfinalrate", Double.parseDouble(request.getParameter("finalrate")));
    	tradeprices getrate3 = crepo.getupdatetransaction((String)session.getAttribute("dclientid"),(String)session.getAttribute("currencypair"),(String)session.getAttribute("dbuysell"),
    			(String)session.getAttribute("dcurrency"),((Double)session.getAttribute("damount")), request.getParameter("day"));

    	model.addAttribute("getrate3",getrate3);
    	model.addAttribute("Hello", (Double)session.getAttribute("damount"));
    	return "getrate3";
    	/*Double res = ((Double)session.getAttribute("damount"));
    	System.out.println(res);
    	model.addAttribute("Hello", (Double)session.getAttribute("damount"));
    	
    	return "getrate3";*/
    	
    }
    /*
    @RequestMapping("/getrate2")
    public String nhadndler(Model model) {
    	final DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
   	 Calendar cal = Calendar.getInstance();
   	 model.addAttribute("hello",(sdf.format(cal.getTime())));
        return "getrate2";
    }
    */
    @RequestMapping("/getrate2")
    public String nhan4dler(Model model) {
    	//Date current = new Date();
    	final DateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
      	 Calendar cal = Calendar.getInstance();
      	 model.addAttribute("hello",(sdf.format(cal.getTime())));   	
        return "getrate2";
    }
    
    @RequestMapping("/new")
    public String nhandler() {
        return "new";
    }
    
    @RequestMapping("/add")
    public String addhandler(HttpServletRequest request,Model model) {
    	Shipper shipper = new Shipper(-1, request.getParameter("shipper"),request.getParameter("phone"));
    	repo.addShipper(shipper);
    	List<Shipper> shippers = repo.allShippers();
        model.addAttribute("shippers", shippers);
    	return "shippers";
    }
    
    
    /* JSP Handler for edit@RequestMapping("/edit")
    public String edithandler(HttpServletRequest request,Model model) {
    	Shipper shipper = repo.getShipper(Integer.parseInt(request.getParameter("id")));
    	model.addAttribute("shipper",shipper);
    	return "edit";
    }*/
    @RequestMapping("/edit/{id}")
	String edithandler(@PathVariable("id") String id,Model model) {
    	Shipper shipper = repo.getShipper(Integer.parseInt(id));
		model.addAttribute("shipper", shipper);
		return "edit";
	}
    
    @RequestMapping("/save")
    public String savehandler(HttpServletRequest request,Model model) {
    	Shipper shipper = new Shipper(Integer.parseInt(request.getParameter("id")),
    			request.getParameter("name"),request.getParameter("phone"));
    	repo.saveShipper(shipper);
    	List<Shipper> shippers = repo.allShippers();
        model.addAttribute("shippers", shippers);
    	return "shippers";
    }
    
    /* JSP Handler for delete @RequestMapping("/delete")
    public String deletehandler(HttpServletRequest request,Model model) {
    	repo.deleteShipper(Integer.parseInt(request.getParameter("id")));
    	List<Shipper> shippers = repo.allShippers();
        model.addAttribute("shippers", shippers);
    	return "shippers";
    }*/
    
    @RequestMapping("/delete/{id}")
   	String deletehandler(@PathVariable("id") String id,Model model) {
    	repo.deleteShipper(Integer.parseInt(id));
    	List<Shipper> shippers = repo.allShippers();
        model.addAttribute("shippers", shippers);
    	return "shippers";
   	}
    
}