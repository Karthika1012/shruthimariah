package com.example.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShipperRepository {
	private ShipperData dao;
	
	@Autowired //or use mysqldirect or jdbtemplate
	public ShipperRepository(ShipperData shipper) {
		this.dao = shipper;
	}
	
	public List<Shipper> allShippers(){
		return dao.allShippers();
	}
	
	public Shipper getShipper(int id) {
		return dao.getShipperById(id);
	}
	
	public int saveShipper(Shipper s) {
		return dao.editShipper(s);
	}
	
	public int addShipper(Shipper s) {
		return dao.addShipper(s);
	}
	
	public int deleteShipper(int id) {
		return dao.deleteShipper(id);
	}
	
}