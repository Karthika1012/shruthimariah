package com.example.business;

import java.util.List;

public interface ShipperData {
	public List<Shipper> allShippers();
	public Shipper getShipperById(int id);
	public int editShipper(Shipper s);
	public int addShipper(Shipper s);
	public int deleteShipper(int id);
}
