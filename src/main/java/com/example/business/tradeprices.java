package com.example.business;



public class tradeprices {
	private String traderid, cpair, basecurrency, termcurrency;	
		private double bid,offer;
	
	public tradeprices(String traderid, String cpair, String basecurrency, String termcurrency, double bid,
			double offer) {
		super();
		this.traderid = traderid;
		this.cpair = cpair;
		this.basecurrency = basecurrency;
		this.termcurrency = termcurrency;
		this.bid = bid;
		this.offer = offer;
		
	}
	public String getTraderid() {
		return traderid;
	}
	public void setTraderid(String traderid) {
		this.traderid = traderid;
	}
	public String getCpair() {
		return cpair;
	}
	public void setCpair(String cpair) {
		this.cpair = cpair;
	}
	public String getBasecurrency() {
		return basecurrency;
	}
	public void setBasecurrency(String basecurrency) {
		this.basecurrency = basecurrency;
	}
	public String getTermcurrency() {
		return termcurrency;
	}
	public void setTermcurrency(String termcurrency) {
		this.termcurrency = termcurrency;
	}
	public double getBid() {
		return bid;
	}
	public void setBid(double bid) {
		this.bid = bid;
	}
	public double getOffer() {
		return offer;
	}
	public void setOffer(double offer) {
		this.offer = offer;
	}
	
		
}
