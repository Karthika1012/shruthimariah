package com.example.business;

import java.util.List;

public interface clientinterface {
	//List<transactions> ListTransactions();	
	List<tradeprices> ListTradePrices(String pair);
	//String ListBaseCurrency(String base);
	double ListFinalRate(String cpair,String buysell,String currency,double amount);
	tradeprices ListUpdateTransaction(String clientid, String cpair,String buysell,String currency,double amount, String date);
}