package com.example.business;
import java.util.Date;

public class transactions {
	private String tid, cid, buycurr, sellcurr, buyorsell;
	private double price, transactionAmount;
	private Date d;
	private int transactionid;
	
	public transactions(int transactionid, String cid, String tid, String buycurr, String sellcurr, String buyorsell, double price,
			double transactionAmount, Date d) {
		super();
		this.tid = tid;
		this.cid = cid;
		this.buycurr = buycurr;
		this.sellcurr = sellcurr;
		this.buyorsell = buyorsell;
		this.price = price;
		this.transactionAmount = transactionAmount;
		this.d = d;
		this.transactionid = transactionid;
	}
	
	public int getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(int transactionid) {
		this.transactionid = transactionid;
	}

	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getBuycurr() {
		return buycurr;
	}
	public void setBuycurr(String buycurr) {
		this.buycurr = buycurr;
	}
	public String getSellcurr() {
		return sellcurr;
	}
	public void setSellcurr(String sellcurr) {
		this.sellcurr = sellcurr;
	}
	public String getBuyorsell() {
		return buyorsell;
	}
	public void setBuyorsell(String buyorsell) {
		this.buyorsell = buyorsell;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public Date getD() {
		return d;
	}
	public void setD(Date d) {
		this.d = d;
	}
	
}
