
	package com.example.business;

	import java.util.List;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Component;

	@Component
	public class clientrepo {
		@Autowired
		clientinterface cli;
		/*public List<transactions> gettransactions() {
			return cli.ListTransactions();
		}*/

		public List<tradeprices> gettradeprices(String pair){
			return cli.ListTradePrices(pair);
		}
		
		/*public String getbasecurrency(String base){
			return cli.ListBaseCurrency(base);
		}*/
		public double getfinalrate(String cpair,String buysell,String currency,double amount){
			return cli.ListFinalRate(cpair,buysell,currency,amount);
		}
		
		public tradeprices getupdatetransaction(String clientid, String cpair,String buysell,String currency,double amount, String date){
			return cli.ListUpdateTransaction(clientid, cpair,buysell,currency,amount, date);
	}
	}

