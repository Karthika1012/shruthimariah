package com.example.dao;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.business.Shipper;

public class ShipperRowMapper implements RowMapper<Shipper> {

	@Override
	public Shipper mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Shipper shipper = new Shipper();
		shipper.setId(rs.getInt("ShipperId"));
		shipper.setName(rs.getString("CompanyName"));
		shipper.setPhone(rs.getString("Phone"));
		return shipper;
	}
}