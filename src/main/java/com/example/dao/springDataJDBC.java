package com.example.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.example.business.*;

@Component
@Qualifier("mysqldirect")
public class springDataJDBC implements ShipperData{
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<Shipper> allShippers() {
		// TODO Auto-generated method stub
		List<Shipper> shippers = template.query(
	            "SELECT ShipperID, CompanyName, Phone From shippers", 
	            new ShipperRowMapper());
	    return shippers;
		
	}

	@Override
	public Shipper getShipperById(int id) {
			
		    // TODO Auto-generated method stub
		    Shipper ashipper = template.queryForObject("SELECT ShipperID, CompanyName, Phone From shippers "
		            + " WHERE ShipperID = ?",new ShipperRowMapper(),id);
		    return ashipper;
		
	}

	@Override
	public int editShipper(Shipper s) {
		// TODO Auto-generated method stub
		return template.update("UPDATE shippers SET CompanyName = ?, Phone = ? WHERE ShipperId = ?",
				s.getName(),s.getPhone(),s.getId());
	}

	@Override
	public int addShipper(Shipper s) {
		// TODO Auto-generated method stub
		return template.update("INSERT INTO shippers (CompanyName,Phone) VALUES (?,?)",
				s.getName(),s.getPhone());
	}

	@Override
	public int deleteShipper(int id) {
		// TODO Auto-generated method stub
		 return template.update("DELETE from shippers where ShipperId = ?",id);
	}

}
