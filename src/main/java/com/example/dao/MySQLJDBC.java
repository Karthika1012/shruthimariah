package com.example.dao;

import java.sql.DriverManager;
import java.sql.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.example.business.clientinterface;
import com.example.business.tradeprices;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;
import com.mysql.jdbc.PreparedStatement;

@Component
public class MySQLJDBC implements clientinterface {
	
@Override
	public List<tradeprices> ListTradePrices(String pair){
		List<tradeprices> prices = new ArrayList<>();		
		ResultSet rs = null;
		Connection cn = null;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, pair);
			rs = st.executeQuery();
			
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				prices.add( new tradeprices(rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6)));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return prices;
	}

	/*@Override
	public String ListBaseCurrency(String base){
		tradeprices basecurren = null;		
		ResultSet rs = null;
		Connection cn = null;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, base);
			rs = st.executeQuery();
			
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				basecurren = new tradeprices(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6));
			}
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return basecurren.getBasecurrency();
	}*/


@Override
public double ListFinalRate(String cpair,String buysell, String currency, double amount) {
	// TODO Auto-generated method stub
	tradeprices traderate = null;		
	ResultSet rs = null;
	Connection cn = null;
	double finalamount = 0;
	double fmt = 0;
	try{
		
		Driver d = new com.mysql.jdbc.Driver();
		DriverManager.registerDriver(d);
		cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
		PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
		st.setString(1, cpair);
		rs = st.executeQuery();
		
		// Process the results of the query, one row at a time
		while (rs.next()) { 
			traderate = new tradeprices(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6));				
		}
		
		if((currency.equals(traderate.getBasecurrency())) && (buysell.equals("buy"))) {
			finalamount = amount / traderate.getOffer();
		}
		else if((currency.equals(traderate.getBasecurrency())) && (buysell.equals("sell"))) {
			finalamount = amount * traderate.getBid();
		}
		else if(currency.equals(traderate.getTermcurrency()) && (buysell.equals("buy"))) {
			finalamount = amount * traderate.getBid();
		}
		else if(currency.equals(traderate.getTermcurrency()) && (buysell.equals("sell"))) {
			finalamount = amount / traderate.getOffer();
		}
		else
			finalamount = 5000;
					
		fmt = Math.round(finalamount * 100D) / 100D;
		
	}
	catch(SQLException ex){
		System.out.println(ex.getMessage());
	}
	finally{
		if(cn != null){
			try{
				if(!cn.isClosed()){
					cn.close();
				}
			}
			catch(SQLException ex){
				System.out.println(ex.getMessage());
			}
		}
	}
		

	return fmt;
}

public tradeprices ListUpdateTransaction(String clientid, String cpair,String buysell,String currency,double amount, String date){
		tradeprices updated = null;		
		ResultSet rs = null;
		Connection cn = null;
		double priced = 0;
		int rows =0;
		try{
			
			Driver d = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(d);
			cn = (Connection) DriverManager.getConnection("jdbc:mysql://mysql.conygre.com:3306/fxtrading?useSSL=false","student","neueda");
			PreparedStatement st = (PreparedStatement) cn.prepareStatement("SELECT * from tradeprice where cpair =?");
			st.setString(1, cpair);
			rs = st.executeQuery();
			
			// Process the results of the query, one row at a time
			while (rs.next()) { 
				updated = new tradeprices(rs.getString(1), rs.getString(2),
						rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6));
			}
			
			PreparedStatement str = (PreparedStatement) cn.prepareStatement("INSERT INTO transactiondetails (ClientID, TraderID, Cpair, DealCurrency, Indicator, Price, DealAmount, TradeDate, ValueDate) values (?,?,?,?,?,?,?,?,?)");
			str.setString(1, clientid);
			str.setString(2,updated.getTraderid());
			str.setString(3,updated.getCpair());
			str.setString(4,currency);
			str.setString(5,buysell);
			if((currency.equals((updated).getBasecurrency())) && (buysell.equals("buy"))) {
				priced = (updated).getOffer();
			}
			else if((currency.equals((updated).getBasecurrency())) && (buysell.equals("sell"))) {
				priced =(updated).getBid();
			}
			else if((currency.equals((updated).getTermcurrency())) && (buysell.equals("buy"))) {
				priced =updated.getBid();
			}
			else if((currency.equals((updated).getTermcurrency())) && (buysell.equals("sell"))) {
				priced =( updated).getOffer();
			}
			else {
				priced = 0;
			}
			str.setDouble(6, priced);
			str.setDouble(7, amount);
			
			java.util.Date utilDate = new java.util.Date();
		    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		    
			str.setDate(8,sqlDate);
			str.setString(9, date);
			
			
			rows = str.executeUpdate();
		}
		catch(SQLException ex){
			System.out.println(ex.getMessage());
		}
		finally{
			if(cn != null){
				try{
					if(!cn.isClosed()){
						cn.close();
					}
				}
				catch(SQLException ex){
					System.out.println(ex.getMessage());
				}
			}
		}
		return updated;
}
}